# L'Univers

- L'Univers a environ 13.8 milliards d'années (13 800 millions d'années, soit 13 800 000 000 années)
- Il y a 13.8 milliards d'années, l'Univers était ultra méga chaud (des milliards de milliards de milliards de degrés) et très dense (tout l'Univers dans une espace presque infiniment petit).
  - Mais l'Univers est en expansion (c'est la théorie du "Big Bang", introduite en 1927 par l’Abbé belge Georges Lemaître, qui propose que l'Univers est en expansion). Expansion ça veut dire qu'on part d'un Univers qui était tout petit et qui devient de plus en plus géant.
- 379 000 années plus tard (il y a 13 799 621 000 ans), l'expansion de l'Univers l'a pas mal refroidit. Il est passé de milliards de milliards de milliards de degrés à environ 3000 degrés. Plein d'hydrogène et de d'hélium se forme (ce sont les premiers atomes). Et grâce à cette température un peu plus "froide" la lumière peut passer.
- (Mais) entre 13.79 et 13.69 milliards d'années (donc pendant 100 millions d'années), il fait encore trop sombre (c'est "l'âge sombre" (dark ages en anglais))
- Sous l'effet de la gravité, les premières étoiles et galaxies (qui contiennent énormément d'étoiles) se forment alors. Les galaxies sont petites, mais grandissent en rencontrant d'autres petits galaxies.
  - Les étoiles les plus grosses implosent, lorsqu'elles sont en fin de vie. Lorsqu'une étoile implose, on appelle ça une "supernova". Les supernovas sont des explosions gigantesques qui dégagent :
    - énormément de lumière
    - énormément d'éléments chimiques (notamment sous la forme de gaz) dans l'espace (dans le milieu interstellaire entre les étoiles)
    - énormément de mouvements (onde de choc), favorisant la formation de nouvelles étoiles
- Et parmi toutes ces galaxies qui se forment dans l'Univers, une nouvelle galaxie se créé, on l'appelle "la Voie Lactée". Cette galaxie, "la Voie Lactée", est la galaxie dans laquelle notre planète Terre se trouve.
  - Les galaxies, elles aussi, grossissent, en "avalant" d'autres galaxies. Notre galaxie La Voie Lactée contient aujourd'hui plus de 100 milliards d'étoiles.
- Il y a environ 4.5 milliards d'années (4 500 000 000 ans) une étoile se forme : le Soleil. Autour du soleil un disque de gaz et de poussière, dans lequel vont se former 8 planètes, dont la Terre.
- Il y a environ 4 milliards d'années, la vie apparaît sur la planète Terre.
- Il y a entre 541 et 530 millions d'années, une quantité incroyable de vie se créé sur la Terre : animaux, végétaux, bactéries. (C'est ce qu'on appelle "l'explosion cambrienne").
- Il y a environ 65 millions d'années, une météorite tombe au Mexique (collision d'un astéroïde de diamètre entre 10,6 et 80,9 km avec la planète Terre). Le diamètre du cratère (cratère de Chicxulub), d’environ 180 km, laisse imaginer une puissance d'explosion similaire à plusieurs milliards de fois celle d'une bombe nucléaire (la bombe d’Hiroshima). Cette explosion, et tout ce qui s'en suivra, est considéré comme une cause *vraisemblable* de l'une des extinctions massives qui ont frappé la Terre, provoquant la disparition progressive des dinosaures, des ptérosaures (reptiles volants) et des plésiosaures (reptiles marins).
- Il y a 7 millions d'années, les premiers ancêtres de la lignée des êtres humains apparaissent.
- L'espèce "Homo erectus" (l'homme dressé, l'homme qui se met debout) est une espèce d'être humain qui se met debout. Les traces les plus anciennes trouvées à ce jour ont été trouvées en Afrique (du Sud)
  - NB : "Homo erectus" n'a pas de définition consensuelle dans la communauté scientifique pour le moment.
- L'homo erectus se lève en Afrique, et depuis, suite à de nombreuses migration, l'humain a peuplé la planète.

## Sources et remerciements

- Eric Lagadec, astrophysicien à l'Observatoire de la Côte d'Azur, spécialiste de poussière d'étoiles. Relations médias à la Société Française d'Astronomie et d'Astrophysique. [Thread twitter](https://twitter.com/EricLagadec/status/1593681169660497925)
- Articles Wikipédia :
  - [Explosion cambrienne](https://fr.wikipedia.org/wiki/Explosion_cambrienne)
  - [Dinosauria](https://fr.wikipedia.org/wiki/Dinosauria)
  - [Cratère de Chicxulub](https://fr.wikipedia.org/wiki/Crat%C3%A8re_de_Chicxulub)
  - [Homo](https://fr.wikipedia.org/wiki/Homo)
  - [Homo erectus](https://fr.wikipedia.org/wiki/Homo_erectus)

Et merci Christian pour l'info !